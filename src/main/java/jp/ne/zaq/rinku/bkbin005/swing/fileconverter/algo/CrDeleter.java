/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.swing.fileconverter.algo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *  @author Mitsutoshi NAKANO bkbin005@rinku.zaq.ne.jp
 *
 */
public class CrDeleter implements Algo {

  @Override
  public void convert(BufferedReader reader, BufferedWriter writer) throws IOException {
    String buf;
    while ((buf = reader.readLine()) != null) {
      writer.write(buf);
      writer.write("\n");
    }
  }
}
