/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.swing.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Mitsutoshi NAKANO bkbin005@rinku.zaq.ne.jp
 */
public class FileSelector extends JPanel implements ActionListener {
  JButton btn =new JButton();
  JTextField path = new JTextField(50);
  File file = null;

  public void create() {
    btn.addActionListener(this);
    this.add(btn);
    path.addActionListener(this);
    this.add(path);
  }

  public void setEditable(boolean b) {
    path.setEditable(b);
  }

  public void setBtnText(String name) {
    btn.setText(name);
  }


  public File getFile() {
    return file;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Object o = e.getSource();
    if (o == btn) {
      showChooser();
    } else if (o == path) {
      changeFile();
    }
  }

  void changeFile() {
    file = new File(path.getText());
  }

  void showChooser() {
    JFileChooser filechooser = new JFileChooser(path.getText());
    int selected = filechooser.showOpenDialog(this);
    if (selected == JFileChooser.APPROVE_OPTION) {
      file = filechooser.getSelectedFile();
      path.setText(file.getPath());
    }
  }
}
