/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.swing.fileconverter.algo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * @author Mitsutoshi NAKANO bkbin005@rinku.zaq.ne.jp
 *
 */
public interface Algo {
  void convert(BufferedReader reader, BufferedWriter writer) throws IOException;
}
