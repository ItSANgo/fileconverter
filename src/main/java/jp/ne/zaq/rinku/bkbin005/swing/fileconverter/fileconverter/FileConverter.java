/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.swing.fileconverter.fileconverter;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import jp.ne.zaq.rinku.bkbin005.swing.component.FileSelector;
import jp.ne.zaq.rinku.bkbin005.swing.fileconverter.algo.Algo;

/**
 * @author Mitsutoshi NAKANO bkbin005@rinku.zaq.ne.jp
 * @see https://www.javadrive.jp/tutorial/
 *
 */
public class FileConverter extends JFrame implements ActionListener {

  private FileSelector from = new FileSelector();
  private FileSelector to = new FileSelector();
  private Algo algo;

  /**
   */
  public void create() {
    setBounds(0, 0, 640, 192);
    from.setBtnText("from");
    from.setEditable(false);
    from.create();
    to.setBtnText("to");
    to.create();
    JButton convert = new JButton();
    convert.setText("convert");
    convert.addActionListener(this);
    JPanel panel = new JPanel();
    panel.add(from);
    panel.add(to);
    panel.add(convert);
    Container contentPane = getContentPane();
    contentPane.add(panel, BorderLayout.CENTER);
    setVisible(true);
  }

  /**
   * @param algo セットする algo
   */
  public void setAlgo(Algo algo) {
    this.algo = algo;
  }

  /* (非 Javadoc)
   * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    try {
      convert(from, to);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  void convert(FileSelector frominfo, FileSelector toinfo) throws IOException {
    convert(frominfo.getFile(), toinfo.getFile());
  }

  void convert(File from, File to) throws IOException {
    convert(from.toPath(), to.toPath());
  }

  void convert(Path from, Path to) throws IOException {
    BufferedReader reader =  Files.newBufferedReader(from);
    BufferedWriter writer = null;
    try {
      writer = Files.newBufferedWriter(to);
      algo.convert(reader, writer);
    } finally {
      if (writer != null) {
        writer.close();
      }
    }
  }
}
