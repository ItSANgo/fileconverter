/**
 *
 */
package jp.ne.zaq.rinku.bkbin005.swing.crdeleter;

import javax.swing.JFrame;

import jp.ne.zaq.rinku.bkbin005.swing.fileconverter.algo.CrDeleter;
import jp.ne.zaq.rinku.bkbin005.swing.fileconverter.fileconverter.FileConverter;

/**
 * @author bkbin
 *
 */
public class DelCr {

  /**
   * @param args
   */
  public static void main(String[] args) {
    FileConverter converter = new FileConverter();
    converter.setAlgo(new CrDeleter());
    converter.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    converter.create();
  }

}
